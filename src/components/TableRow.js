import React from 'react'

/**
 * Stateless component which takes in props from
 * it's parent and returns a table row
 * @param  {[type]} props [description]
 * @return {[type]}       [description]
 */
const TableRow = (props) => {
	return (
			<tbody>
			{
				props.booking.cancelled ? (
					<tr className="cancelled" onClick={props.index}>
					 	<td colSpan="3">{props.booking.title} {props.booking.firstName} {props.booking.lastName}</td>
					 	<td>{props.booking.time}</td>
					 	<td>{props.booking.partySize}</td>
					 	<td>
					 	{
					 		props.booking.seated ? (
								<div>{'Y'}</div>
						  ) : 
						  	<div>{'N'}</div>
					 	}
					 	</td>
				 	</tr>
				) : 
					<tr onClick={props.index}>
					 	<td colSpan="3">{props.booking.title} {props.booking.firstName} {props.booking.lastName}</td>
					 	<td>{props.booking.time}</td>
					 	<td>{props.booking.partySize}</td>
					 	<td>
					 	{
					 		props.booking.seated ? (
								<div>{'Y'}</div>
						  ) : 
						  	<div>{'N'}</div>
					 	}
					 	</td>
				 	</tr>
			}
			</tbody>
	);
}

export default TableRow;
