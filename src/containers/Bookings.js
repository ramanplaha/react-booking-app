/* Importing external libraries */
import React, { Component } from 'react';
import Moment from 'react-moment';
/* Importing internal files */
import TableRow from '../components/TableRow';
import Panel from '../components/Panel';


class Bookings extends Component {
	constructor() {
		super()

		/**
		 * Set our default state
		 * @type {Object}
		 */
		this.state = {
			data: []
		}
	}

	/**
	 * Use componentDidMount lifecycle hook to execute 
	 * our fetch method that retrieves JSON date from
	 * our endpoint
	 * @return [Array]
	 */
	componentDidMount() {
		fetch('data/bookings.json')
			.then(res => res.json())
			.then(data => this.setState({
				data: data[0]
			}));
	}

	/**
	 * Use the index passed to the function to get
	 * the active booking in the existing data.bookings state
	 * @param  number
	 */
	getBookingInfo(index) {
		const activeBookingInfo = this.state.data.bookings[index]
		this.setState({activeBookingInfo});

		/**
		 * A conditional statement to check if '.books__panel' exists
		 * If it exists and it contain a class of hidden,
		 * then toggle it
		 */
		if (!(document.querySelector('.bookings__panel') === null)) {
			if (document.querySelector('.bookings__panel').classList.contains('hidden')) {
				this.togglePanel();
			}
		}
	}

	/**
	 * Here we handle the onChange events passed down when the user
	 * changes the radio button input. We check to see if the user is
	 * changing 'isSeated', or 'isCancelled'. Then we reassign the correct
	 * boolean and setState again
	 * @param  {Object}
	 * @return {Object}
	 */
	handleChange(event) {
		if (event.target.name === 'isSeated') {
			event.target.value === 'true' ? 
				this.state.activeBookingInfo.seated = true 
			: this.state.activeBookingInfo.seated = false

			this.setState({activeBookingInfo: this.state.activeBookingInfo})

		} else if (event.target.name === 'isCancelled') {
			event.target.value === 'false' ?
				this.state.activeBookingInfo.cancelled = true
			: this.state.activeBookingInfo.cancelled = false

			this.setState({activeBookingInfo: this.state.activeBookingInfo})
		}
	}

	/**
	 * Toggles the hidden class on '.bookings__panel'
	 */
	togglePanel() {
		const bookingsPanel = document.querySelector('.bookings__panel');
		bookingsPanel.classList.toggle('hidden');
	}

	render() {
		return (
			<div className="bookings">
				{
					this.state.data.bookings &&
							
						<div className="bookings__table">
							<h1>Booking for <Moment format="DD/MM/YYYY">{ this.state.data.date }</Moment></h1>
							<table>
							  <thead>
							    <tr>
							      <th colSpan="3">Name</th>
							      <th>Time</th>
							      <th>Covers</th>
							      <th>Seated</th>
							    </tr>
							  </thead> 
							{ 
								this.state.data.bookings.map((booking,index) =>
									<TableRow key={ index } booking={ booking } index={ () => this.getBookingInfo(index) } />
								)
							}
							</table> 
						</div> 
				}
				{
					this.state.activeBookingInfo &&
						<div className="bookings__panel">
							<h1>Booking update</h1>
							<div className="panel">
								<div className="panel__close" onClick={this.togglePanel.bind(this)}></div>
								<div className="panel__item">
									<h2>Name</h2>
									<p>{this.state.activeBookingInfo.title} {this.state.activeBookingInfo.firstName} {this.state.activeBookingInfo.lastName}</p>
								</div>
								<div className="panel__item">
									<h2>Time</h2>
									<p>{this.state.activeBookingInfo.time}</p>
								</div>
								<div className="panel__item">
									<h2>Covers</h2>
									<p>{this.state.activeBookingInfo.partySize}</p>
								</div>
							  <div className="panel__item">
			  					<h2>Seated</h2>
									{
			  						this.state.activeBookingInfo.cancelled === true ?
			  							<div></div>
			  						: 
			  							<div>
				  							<label>
						  						<input 
						  							type="radio" 
						  							name="isSeated" 
						  							checked={this.state.activeBookingInfo.seated == true} 
						  							onChange={this.handleChange.bind(this)} 
						  							value={true} />Yes
						  					</label>
						  					<label>
						  				  	<input 
						  				  		type="radio" 
						  				  		name="isSeated" 
						  				  		checked={this.state.activeBookingInfo.seated == false} 
						  				  		onChange={this.handleChange.bind(this)} 
						  				  		value={false}  /> No
						  				  </label>
					  				  </div>
			  					}

			  					{
			  						this.state.activeBookingInfo.seated === true ?
			  							<div></div>
			  						: 
			  							<label>
			  								<input 
			  									type="radio" 
			  									name="isCancelled" 
			  									checked={this.state.activeBookingInfo.cancelled == true} 
			  									onChange={this.handleChange.bind(this)} 
			  									value={false}  />Cancelled
			  							</label>
			  					}
		  				  	
			  				</div>
			  				<div className="panel__item">
			  					<h2>Notes</h2>
			  					{
			  						this.state.activeBookingInfo.notes === '' ? (
			  							<p>{'No Notes'}</p>
			  					  ) : 
			  					  	<p>{ this.state.activeBookingInfo.notes }</p>
			  					}
			  				</div>
							</div>
						</div>
						// <Panel activeBooking={ this.state.activeBookingInfo } status={() => console.log('Hit') }/>
				}
			</div>
		)
	}
}
export default Bookings;
