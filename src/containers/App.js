/* Importing external libraries */
import React, { Component } from 'react';
/* Importing internal files */
import Header from '../components/Header';
import Bookings from './Bookings';
import css from '../styles/App.scss';

export default class App extends Component {
  render() {
    return (
    	<div>
	      <Header />
		    <Bookings />
      </div>
    );
  }
}
